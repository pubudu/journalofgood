class AddPublishToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :publish, :boolean, :default => true
  end
end
