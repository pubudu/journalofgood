class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.text :content
      t.integer :user_id
      t.boolean :anonymous

      t.timestamps
    end
  end
end
