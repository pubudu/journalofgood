class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@journalofgood.org"
  def invitation_email(email, url, inviter)
    @url = url
    @inviter = inviter
    mail(to: email, subject: 'You have been invited to Journal Of Good!')
  end

  def password_reset_email(email, password)
    @password = password
    mail(to: email, subject: 'Password reset instructions.')
  end

  def contact_email(email, content)
    @email = email
    @content   = content
    mail(to:Rails.application.config.contact_email, subject:'Contact Form' )
  end
end
