class PasswordResetEmail
  include ActiveModel::Model
  attr_accessor :email
  validates :email, presence: true, :email => true
  validates_each :email do |record, attr, value|
    if record.errors.blank?
      record.errors.add(attr,"No account associated with email") unless User.find_by_email(value)
    end
  end
end