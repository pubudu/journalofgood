class User < ActiveRecord::Base
  include EmailValidation
  include PasswordValidation

  has_many :invitations
  has_many :entries
  belongs_to :inviter, class_name: "User"
  has_many :invitees, class_name: "User", foreign_key: "inviter_id"

  validates :email, uniqueness: true
  validates :name, presence: true, uniqueness: true, length: { minimum: 3 , maximum: 40}

  before_save :encrypt_password
  def encrypt_password
    if password_changed?
      self.password = PasswordHash.createHash(self.password)
    end
  end
end
