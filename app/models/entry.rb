class Entry < ActiveRecord::Base
  belongs_to :user
  has_paper_trail :only => [:content]
  acts_as_taggable_on :journals
  self.per_page = 10
  validates :content, presence: true
  validate :entry_limit
  def entry_limit
    if Entry.where(:user_id => user_id, :created_at => ((Time.now - 1.day)..Time.now)).size > 20
      errors[:base] << "You have exceeded the rate at which you can create entries!"
    end
  end
end
