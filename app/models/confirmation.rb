class InvitationConfirmationValidator < ActiveModel::Validator
  def validate(record)
    invitation = Invitation.find_by_url_suffix(record.id)
    record.errors.clear if not invitation or invitation.materialized? or User.find_by_email(invitation.email)
    if not invitation
      record.errors[:base] << "Invitation does not exist"
    elsif invitation.materialized?
      record.errors[:base] << "Invitation already materialized"
    elsif User.find_by_email(invitation.email)
      record.errors[:base] << "Account has been already created for the invitation email address"
    end
  end
end

class Confirmation
  include ActiveModel::Model
  include PasswordValidation
  attr_accessor :password, :password_confirmation, :id, :user_name

  validates :password, confirmation: true
  validates :user_name, presence: true, length: { minimum: 6 , maximum: 40}
  validates_with InvitationConfirmationValidator
  def persisted?
    #we return true because we always want to update an existing confirmation, not create a new one
    true
  end
end
