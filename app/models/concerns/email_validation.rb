module EmailValidation
  extend ActiveSupport::Concern
  included do
    validates :email, presence: true, :email => true,  length: { maximum: 60}
  end
end 