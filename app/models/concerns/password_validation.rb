module PasswordValidation
  extend ActiveSupport::Concern
  included do
    validates :password, presence: true,  length: { minimum: 6}
  end
end