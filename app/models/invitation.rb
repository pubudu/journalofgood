class Invitation < ActiveRecord::Base
  include EmailValidation
  belongs_to :user
  before_create do
    self.url_suffix = SecureRandom.uuid
  end

  validate :invitation_limit

  def invitation_limit
    if Invitation.where(:user_id => user_id).size > 20
      errors[:base] << "You have exceeded the your 20 invitations!"
    end
  end
end
