class SesssionValidator < ActiveModel::Validator
  def validate(record)
    user = User.find_by_email(record.email)
    if user and PasswordHash.validatePassword( record.password, user.password )
      record.id = user.id
    else
      record.errors[:base] << "Invalid email or password" if record.errors.blank?
    end
  end
end

class Session
  include ActiveModel::Model
  attr_accessor :email, :password, :id
  validates :email, presence: true, :email => true
  validates :password, presence: true
  validates_with SesssionValidator
end

