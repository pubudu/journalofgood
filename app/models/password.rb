class Password
  include ActiveModel::Model
  include PasswordValidation
  attr_accessor :password, :password_confirmation
  validates :password, confirmation: true
  def persisted?
    #we return true because we always want to update the password of an existing user
    true
  end
end