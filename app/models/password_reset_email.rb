class PasswordResetEmail
  include ActiveModel::Model
  attr_accessor :email
  validate :account_exists
  def account_exists
    errors.add(:email , "No account associated with email") unless User.find_by_email(email)
  end
end