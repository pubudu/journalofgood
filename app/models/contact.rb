class Contact
  include ActiveModel::Model
  include EmailValidation
  attr_accessor :email, :content
  validates :content, presence: true
end