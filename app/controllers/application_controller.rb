class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :loggedin
  def authenticate
    unless session[:user_id]
      redirect_to sessions_new_path
    end
  end

  def loggedin
    return session[:user_id]
  end

  def current_user
    return session[:user_id]
  end
  
  def disallow_loggedin_user
    redirect_to entries_path if session[:user_id]
  end
end
