class PasswordResetEmailsController < ApplicationController
  before_action :disallow_loggedin_user,only:[:new, :create]

  def new
    @reset_email = PasswordResetEmail.new
  end

  def create
    @reset_email = PasswordResetEmail.new(params.require(:password_reset_email).permit(:email))
    if verify_recaptcha(:model => @reset_email) and @reset_email.valid?
      new_password = rand(36**18).to_s(36)
      User.find_by_email(@reset_email.email).update(password: new_password)
      ApplicationMailer.password_reset_email(@reset_email.email, new_password).deliver
      redirect_to sessions_new_path, notice: "Please follow the instructions sent to you via email."
    else
      render action: 'new'
    end
  end
end