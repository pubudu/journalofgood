class PasswordsController < ApplicationController
  before_action :authenticate
  def edit
    @password = Password.new
  end

  def update
    @password = Password.new(params.require(:password).permit(:password, :password_confirmation))
    if @password.valid?
      User.find(session[:user_id]).update(password: @password.password)
      session.delete(:user_id)
      redirect_to sessions_new_path, notice: "Password changed"
    else
      render action: 'edit'
    end
  end
end