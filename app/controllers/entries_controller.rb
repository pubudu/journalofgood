class EntriesController < ApplicationController
  before_action :set_entry, only: [:show, :edit, :update, :destroy]
  before_action :authenticate, except: [:journal, :all]
  before_action :default_page, only: [:index,:journal,:all]
  # GET /entries
  # GET /entries.json
  def index
    @entry = Entry.new(:anonymous => true, :publish=>false)
    @entries = Entry.where(user_id: session[:user_id]).order(id: :desc).paginate(:page => params[:page])
  end

  def journal
    @entries = Entry.where(publish: true).tagged_with(params[:journal], :on => :journals).order(id: :desc).paginate(:page => params[:page])
  end
  
  def all
    @entries = Entry.where(publish: true).order(id: :desc).paginate(:page => params[:page])
  end

  # GET /entries/1
  # GET /entries/1.json
  def show
  end

  # GET /entries/1/edit
  def edit
  end

  # POST /entries
  # POST /entries.json
  def create
    @entry = Entry.new(entry_params)
    @entry.user_id = session[:user_id]
    if @entry.save
      redirect_to @entry, notice: 'Entry was successfully created.'
    else
      @entries = Entry.where(user_id: session[:user_id]).order(id: :desc).paginate(:page => params[:page])
      render action: 'index'
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    if @entry.update(entry_params)
      redirect_to @entry, notice: 'Entry was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    @entry.destroy
    redirect_to entries_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_entry
    @entry = Entry.find(params[:id])
    if @entry.user_id != session[:user_id]
      render :text => 'Not Found', :status => '404'
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def entry_params
    params.require(:entry).permit(:content, :anonymous, :journal_list, :publish)
  end
  
  def default_page
    params[:page] = 1 unless params[:page]
  end
end
