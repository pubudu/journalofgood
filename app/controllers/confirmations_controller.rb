class ConfirmationsController < ApplicationController
  before_action :disallow_loggedin_user,only:[:edit, :update]
  def edit
    @confirmation = Confirmation.new
    @confirmation.id = params['id']
  end

  def update
    @confirmation = Confirmation.new(params.require(:confirmation).permit(:password, :password_confirmation, :user_name))
    @confirmation.id = params[:id]
    if @confirmation.valid?
      invitation = Invitation.find_by_url_suffix(@confirmation.id)
      invitation.update(materialized: true)
      User.new(:email=>invitation.email,:password=>@confirmation.password, :name=>@confirmation.user_name, :inviter_id=>invitation.user_id).save
      redirect_to sessions_new_path, notice: "Account succefully created!"
    else
      render action: 'edit'
    end
  end
end
