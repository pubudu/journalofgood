class SessionsController < ApplicationController
  before_action :disallow_loggedin_user,only:[:new, :create]
  def new
    @session = Session.new
  end

  def show
    render :layout => false
  end

  def create
    @session = Session.new(session_params)
    if @session.valid?
      reset_session
      session[:user_id] = @session.id
      redirect_to entries_path
    else
      render action: 'new'
    end
  end

  def destroy
    session.delete(:user_id)
    redirect_to sessions_new_path
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def session_params
    params.require(:session).permit(:email, :password)
  end
end
