class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params.require(:contact).permit(:email, :content))
    if verify_recaptcha(:model => @contact) and @contact.valid?
      ApplicationMailer.contact_email(@contact.email, @contact.content).deliver
      redirect_to root_path, notice: "Thank you for contacting us!"
    else
      render action: 'new'
    end
  end
end