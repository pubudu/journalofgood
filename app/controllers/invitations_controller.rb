class InvitationsController < ApplicationController
  before_action :authenticate
  # GET /invitations
  # GET /invitations.json
  def index
    @invitation = Invitation.new
    @user = User.find_by_id(session[:user_id])
    @invitations = @user.invitations.order(id: :desc)
  end


  # POST /invitations
  # POST /invitations.json
  def create
    @invitation = Invitation.new(params.require(:invitation).permit(:email))
    @invitation.user_id = session[:user_id]
    @user = User.find_by_id(session[:user_id])
    if verify_recaptcha(:model => @invitation) && @invitation.save
      url = url_for(:controller=>'confirmations',:action=>'edit', :id=>@invitation.url_suffix)
      ApplicationMailer.invitation_email(@invitation.email, url, @user.name).deliver
      @invitation = Invitation.new
    end
    @invitations = @user.invitations.order(id: :desc)
    render action: 'index'
  end
end
